package com.dataart.seadvanced;

import com.dataart.seadvanced.server.MessageTypes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Client {

    private String name;
    private Socket socket;

    public Client(String name) {
        this.name = name;
    }

    public void connect(String host, int port) throws IOException {
        socket = new Socket(host, port);
        send(name, MessageTypes.AUTH);
        readResponse();
    }

    public void send(String request) throws IOException {
        send(request, MessageTypes.MESSAGE);
    }

    public void send(String request, MessageTypes type) throws IOException {
        OutputStream stream;
        try {
            stream = socket.getOutputStream();
        } catch (IOException e) {
            System.out.println("Unable to write to socket");
            return;
        }

        switch (type) {
            case AUTH:
                stream.write(type.getValue());
                break;
            case MESSAGE:
                stream.write(type.getValue());
                break;

        }
        stream.write(ByteBuffer.allocate(Integer.BYTES).putInt(request.length()).array());
        stream.write(request.getBytes());
    }

    public String readResponse() throws IOException {
        InputStream inputStream;
        try {
            inputStream = socket.getInputStream();
        } catch (IOException e) {
            System.out.println("Unable to get input stream: " + e.getMessage());
            return null;
        }

        int length = ByteBuffer.wrap(read(inputStream, Integer.BYTES)).getInt();
        return new String(read(inputStream, length));
    }

    protected byte[] read(InputStream inputStream, int length) throws IOException {
        byte[] buffer = new byte[length];
        {
            int read = 0;
            do {
                read += inputStream.read(buffer, read, length - read);
            } while (length > read);
        }

        return buffer;
    }

}
