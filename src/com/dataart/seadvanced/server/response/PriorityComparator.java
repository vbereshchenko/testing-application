package com.dataart.seadvanced.server.response;

import java.util.Comparator;

public class PriorityComparator implements Comparator<Response> {

    @Override
    public int compare(Response o1, Response o2) {

        if (o1.getPriority() > o2.getPriority()) {
            return -1;
        }

        if (o1.getPriority() < o2.getPriority()) {
            return 1;
        }

        return 0;
    }
}
