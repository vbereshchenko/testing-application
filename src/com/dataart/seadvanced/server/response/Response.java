package com.dataart.seadvanced.server.response;

public interface Response {
    String getResponse(String request);

    int getPriority();
}
