package com.dataart.seadvanced.server.response;

public class ResponseImpl implements Response {
    private int priority = 1;

    protected void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String getResponse(String request) {
        return null;
    }

    @Override
    public int getPriority() {
        return priority;
    }
}
