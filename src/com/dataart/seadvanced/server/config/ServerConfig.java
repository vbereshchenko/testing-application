package com.dataart.seadvanced.server.config;

public class ServerConfig implements Config {
    private int port;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
