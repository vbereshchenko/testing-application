package com.dataart.seadvanced.server;

import com.dataart.seadvanced.server.config.ClientConfig;
import com.dataart.seadvanced.server.config.Config;
import com.dataart.seadvanced.server.config.ServerConfig;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class IncomeParameters {

    private Map<String, String> configuration = new HashMap<String, String>();

    public static IncomeParameters createFromParameters(String[] params) {
        IncomeParameters conf = new IncomeParameters();
        for (int i = 0; i < params.length; i++) {
            String current = params[i];
            String key, value = "1";
            if (current.startsWith("-")) {
                key = current;
            } else {
                continue;
            }
            if (params.length == i + 1 || params[i + 1].startsWith("-")) { // current element is last
                conf.set(key.substring(1), value);
                continue;
            }
            value = params[i + 1];
            conf.set(key.substring(1), value);
            i++;
        }

        return conf;
    }

    private IncomeParameters set(String key, String value) {
        configuration.put(key, value);

        return this;
    }

    public String get(String key) {
        if (!configuration.containsKey(key)) {
            return null;
        }

        return configuration.get(key);
    }

    public boolean contains(String key) {
        return configuration.containsKey(key);
    }

    public Config parseConfig() throws IOException, SAXException, ParserConfigurationException {
        File configFile = new File(get("config"));
        if (!configFile.exists()) {
            throw new FileNotFoundException("Configuration file is not found");
        }
        SAXParserFactory factory = SAXParserFactory.newInstance();

        SAXParser saxParser = null;
        saxParser = factory.newSAXParser();

        if (contains("client")) {
            ClientConfig config = new ClientConfig();
            SAXConfigParser<ClientConfig> handler = new SAXConfigParser<ClientConfig>(config);
            saxParser.parse(new FileInputStream(configFile), handler);

            return config;
        } else {
            ServerConfig config = new ServerConfig();
            SAXConfigParser<ServerConfig> handler = new SAXConfigParser<ServerConfig>(config);
            saxParser.parse(new FileInputStream(configFile), handler);

            return config;
        }
    }

    private static class SAXConfigParser<T extends Config> extends DefaultHandler {
        private T config;

        public SAXConfigParser(T config) {
            this.config = config;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("server") && config instanceof ServerConfig) {
                ((ServerConfig) config).setPort(Integer.valueOf(attributes.getValue("port")));
            } else if (qName.equals("client") && config instanceof ClientConfig) {
                ClientConfig current = (ClientConfig) config;
                current.setPort(Integer.valueOf(attributes.getValue("port")));
                current.setHost(attributes.getValue("host"));
            }
        }

    }
}
