package com.dataart.seadvanced.server;

import com.dataart.seadvanced.client.AuthRequest;
import com.dataart.seadvanced.client.MessageRequest;
import com.dataart.seadvanced.client.Request;
import com.dataart.seadvanced.server.response.Response;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.PriorityQueue;

public class ClientConnection implements Runnable {

    public static final String EXIT_MESSAGE = "quit";
    private static final String EXIT_SERVER_MESSAGE = "quit-server";

    private String name;
    private Socket socket;
    private Server server;
    private PriorityQueue<Response> listeners;

    public ClientConnection(Server server, Socket socket, PriorityQueue<Response> listeners) {
        this.socket = socket;
        this.listeners = listeners;
        this.server = server;
    }

    @Override
    public void run() {
        try {
            startCommunication();
        } catch (IOException e) {
            System.out.println("Communication failed: " + e.getMessage());
            closeClient();
        }
    }

    private void startCommunication() throws IOException {
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();

        String message;
        do {
            Request request = readRequest(inputStream);

            if (request == null) {
                System.out.println("End of communication detected");
                closeClient();
                break;
            }

            if (request instanceof MessageRequest) {
                System.out.println(
                        Thread.currentThread().getId() +
                                " - " + name + ": " +
                                ((MessageRequest) request).getMessage());
            }


            if (request.toString().equals(EXIT_SERVER_MESSAGE)) {
                stopServer();
                break;
            }

            message = generateResponse(request);

            if (request.toString().equals(EXIT_MESSAGE)) {
                message = "Good bye!";
            }

            System.out.println(Thread.currentThread().getId() + " - Server to " + name + ": " + message);
            sendResponse(outputStream, message);

            if (message == null) {
                break;
            }
        } while (!message.equals(EXIT_MESSAGE));
        System.out.println("User with name '" + name + "' lost connection");

        closeClient();
    }

    private void sendResponse(OutputStream output, String response) throws IOException {
        BufferedOutputStream outputStream = new BufferedOutputStream(output);
        outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(response.length()).array());
        outputStream.write(response.getBytes());
        outputStream.flush();
    }

    private Request readRequest(InputStream input) throws IOException {
        int type = input.read();
        if (type == -1) {
            return null;
        }

        byte[] buffer = read(input, Integer.BYTES);

        if (buffer == null) {
            return null;
        }

        int actualLength = ByteBuffer.wrap(buffer).getInt();

        buffer = read(input, actualLength);

        String content = new String(buffer);

        Request request;

        if (type == MessageTypes.AUTH.getValue()) {
            request = new AuthRequest(content);
        } else {
            request = new MessageRequest(content);
        }

        return request;
    }

    private String generateResponse(Request request) {

        String response = null;

        if (request instanceof AuthRequest) {
            String username = ((AuthRequest) request).getUsername();
            this.name = username;
            System.out.println("User with name " + username + " has just connected");
            response = "Welcome, " + username + "!";
        } else {
            String message = ((MessageRequest) request).getMessage();
            for (Response listener : listeners) {
                String temp = listener.getResponse(message);
                if (response == null) {
                    response = temp;
                }
            }
        }

        if (response == null) {
            response = "Unable to find response";
        }

        return response;
    }

    private byte[] read(InputStream inputStream, int length) throws IOException {
        byte[] buffer = new byte[length];
        int read = 0;
        do {
            int current = inputStream.read(buffer, read, length - read);
            if (current == -1) {
                break;
            }
            read += current;
        } while (length > read);

        return buffer;
    }

    private void stopServer() throws IOException {
        server.stop();
    }

    private void closeClient() {
        if (socket.isClosed()) {
            return;
        }

        try {
            socket.close();
        } catch (IOException e) {
            System.out.println("Can't close client connection:" + e.getMessage());
        }
    }

}
