package com.dataart.seadvanced.server;

public enum MessageTypes {
    AUTH(1), MESSAGE(2);

    private final int id;

    MessageTypes(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}
