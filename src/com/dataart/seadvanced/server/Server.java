package com.dataart.seadvanced.server;

import com.dataart.seadvanced.server.response.PriorityComparator;
import com.dataart.seadvanced.server.response.Response;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.PriorityQueue;

public class Server {

    private String host;
    private int port;
    private PriorityQueue<Response> listeners = new PriorityQueue<Response>(new PriorityComparator());
    private ServerSocket server;

    public Server(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Server addListener(Response response) {
        listeners.add(response);

        return this;
    }

    public void start() {
        if (listeners.size() == 0) {
            System.out.println("Unable to start server. You must declare at least one listener");
            return;
        }

        try {
            server = createServer();
        } catch (IOException e) {
            System.out.println("Unable start listening: " + e.getMessage());
            return;
        }

        while (true) {

            if (server.isClosed()) {
                break;
            }
            Socket socket;
            try {
                socket = server.accept();
            } catch (IOException e) {
                if (!server.isClosed()) {
                    System.out.println("Unable to handle connection from user: " + e.getMessage());
                }
                continue;
            }

            ClientConnection user = new ClientConnection(this, socket, listeners);

            new Thread(user).start();
        }

        System.out.println("Server work was terminated");

    }

    private ServerSocket createServer() throws IOException {
        InetAddress address = InetAddress.getByName(this.host);
        return new ServerSocket(this.port, 100, address);
    }

    public void stop() throws IOException {
        server.close();
    }

}
