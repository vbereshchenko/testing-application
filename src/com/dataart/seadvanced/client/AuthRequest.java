package com.dataart.seadvanced.client;

public class AuthRequest extends Request {
    public AuthRequest(String username) {
        super(username);
    }

    public String getUsername() {
        return getContent();
    }
}
