package com.dataart.seadvanced.client;

public class Request {

    private String content = "";

    public Request(String content) {
        this.content = content;
    }

    protected String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return content;
    }
}
