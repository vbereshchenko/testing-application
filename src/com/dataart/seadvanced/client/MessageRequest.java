package com.dataart.seadvanced.client;

public class MessageRequest extends Request {
    public MessageRequest(String message) {
        super(message);
    }

    public String getMessage() {
        return getContent();
    }
}
