package com.dataart.seadvanced;

import com.dataart.seadvanced.server.IncomeParameters;
import com.dataart.seadvanced.server.Server;
import com.dataart.seadvanced.server.config.ClientConfig;
import com.dataart.seadvanced.server.config.Config;
import com.dataart.seadvanced.server.config.ServerConfig;
import com.dataart.seadvanced.server.response.ResponseImpl;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {

        IncomeParameters config = IncomeParameters.createFromParameters(args);

        Config conf;

        try {
            conf = config.parseConfig();
        } catch (FileNotFoundException e) {
            System.out.println("Unable to load configuration file: " + e.getMessage());
            return;
        } catch (SAXException e) {
            System.out.println("Error happened: " + e.getMessage());
            return;
        } catch (ParserConfigurationException e) {
            System.out.println("Error with parser configuration" + e.getMessage());
            return;
        } catch (IOException e) {
            System.out.println("Stream error: " + e.getMessage());
            return;
        }

        if (config.contains("client")) {
            startClient((ClientConfig) conf);
        } else if (config.contains("server")) {
            startServer((ServerConfig) conf);
        } else {
            System.out.println("Unknown starting type");
        }
    }

    private static void startClient(ClientConfig config) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("What's your name? ");
        Client user;
        try {
            user = new Client(reader.readLine());
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
            return;
        }
        try {
            user.connect(config.getHost(), config.getPort());
        } catch (IOException e) {
            System.out.println("Unable to connect: " + e.getMessage());
            return;
        }

        String response;
        String request = "";
        do {
            System.out.print("Request: ");
            try {
                request = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {

                user.send(request);

                response = user.readResponse();
            } catch (IOException e) {
                System.out.println("Server closed connection unexpectedly");
                break;
            }

            System.out.println("Response: " + response);
        } while (!request.equals("quit"));
    }

    private static void startServer(ServerConfig config) {
        System.out.println("Listening " + config.getPort());
        Server server = new Server("0.0.0.0", config.getPort());
        server.addListener(new ResponseImpl() {
            {
                setPriority(15);
            }

            private int count = 0;

            @Override
            public String getResponse(String request) {
                if (request.equals("getCount")) {
                    return "" + count;
                }
                count++;
                return null;
            }
        });
        server.addListener(new ResponseImpl() {
            {
                setPriority(10);
            }

            @Override
            public String getResponse(String request) {
                if (request.equals("Hi!")) {
                    return "Hi, how're you?";
                } else if (request.equalsIgnoreCase("I'm fine. and you?")) {
                    return "I'm ok";
                }

                return "What?";
            }
        });
        server.start();
    }
}
