package com.dataart.seadvanced;

import com.dataart.seadvanced.server.Server;
import com.dataart.seadvanced.server.response.ResponseImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Test {

    private static final int THREAD_COUNT = 15;
    private static final int COMMANDS_PER_USER = 10;

    private static final int PORT = 8081;

    public static void main(String[] args) {

        Test test = new Test();
        test.initialize();
        test.testMessaging();

    }

    public void testMessaging() {
        final Service service = new Service(THREAD_COUNT);

        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(new Worker(i, service)).start();
        }
    }

    private void initialize() {
        final Server server = new Server("0.0.0.0", PORT);
        server.addListener(new ResponseImpl() {
            private int count = 1;
            {
                setPriority(15);
            }

            @Override
            public String getResponse(String request) {
                if (request.equals("getCount")) {
                    return "" + count;
                }
                count++;
                return null;
            }
        });
        server.addListener(new ResponseImpl() {
            {
                setPriority(10);
            }
            @Override
            public String getResponse(String request) {
                return "OK";
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                server.start();
            }
        }).start();
    }

    private static class Service {

        private CyclicBarrier barrier;
        private List<String> queue = new ArrayList<String>();

        public Service(int threads) {
            barrier = new CyclicBarrier(threads, new Runnable() {
                private int usedId = 1;

                @Override
                public void run() {
                    Client countChecker = new Client("Count_Checker");
                    try {
                        countChecker.connect("127.0.0.1", 8081);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                    int count;
                    try {
                        countChecker.send("getCount");
                        String response = countChecker.readResponse();
                        count = Integer.valueOf(response);
                    } catch (IOException e) {
                        System.out.println("Server closed connection: " + e.getMessage());
                        return;
                    }
                    System.out.println("Filling " + queue);
                    // checking if message received equals to messages sent
                    if (queue.size() == THREAD_COUNT && (count/(THREAD_COUNT * usedId++)) == 1) {
                        System.out.println("Test passed");
                    } else {
                        System.out.println("Test is not passed: threads needed: " + THREAD_COUNT + " and got: " + queue.size());
                    }
                    queue.clear();
                }
            });
        }

        public void recharge(String name) {
            queue.add(name);
            try {
                barrier.await();
            } catch (InterruptedException e) {
                System.out.println("InterrupedException: " + e.getMessage());
            } catch (BrokenBarrierException e) {
                System.out.println("BrokenBarrierException: " + e.getMessage());
            }
        }
    }

    private static class Worker implements Runnable {

        private String name;
        private Service service;

        private Worker(int name, Service service) {
            this.name = String.valueOf(name);
            this.service = service;
        }

        @Override
        public void run() {
            Client user = new Client(String.valueOf(name));
            try {
                user.connect("127.0.0.1", PORT);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            for (int i = 0; i < COMMANDS_PER_USER; i++) {
                String result;
                try {
                    user.send(String.valueOf(i + 1));
                    result = user.readResponse();
                } catch (IOException e) {
                    System.out.println("Unable to send request to server");
                    return;
                }
                if (result.equals("OK")) {
                    service.recharge("OK");
                } else {
                    System.out.println("Error");
                }
            }
        }
    }
}
